﻿namespace OtusHomeWork.PrototypPattern
{
    public interface IMyCloneable<out T>
    {
        T Copy();
    }
}