﻿namespace OtusHomeWork.PrototypPattern
{
    /// <summary>
    /// Хищник
    /// </summary>
    public class PredatorAnimal : Animal, IMyCloneable<PredatorAnimal>
    {
        /// <summary>
        /// Количество зубов
        /// </summary>
        /// <returns></returns>
        private int _numberOfTeeth;
        public int NumberOfTeeth
        {
            get => _numberOfTeeth;
            set => _numberOfTeeth = value;
        }

        public PredatorAnimal(int height, int weight, string family, int numberOfTeeth) 
            : base(height, weight, family)
        {
            _numberOfTeeth = numberOfTeeth;
        }
        
        public PredatorAnimal Copy()
        {
            return new PredatorAnimal(_height, _weight, _family, _numberOfTeeth);
        }
        
        public override string ToString()
        {
            return $"PredatorAnimal: {_family}, {_numberOfTeeth}, {_height}, {_weight}";
        }

        public override object Clone()
        {
            return new PredatorAnimal(_height, _weight, _family, _numberOfTeeth);
        }
    }
}