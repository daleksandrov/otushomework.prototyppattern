﻿using System.Collections;
using System.Collections.Generic;

namespace OtusHomeWork.PrototypPattern
{
    /// <summary>
    /// Живое существо
    /// </summary>
    public abstract class LivingEntity
    {
        /// <summary>
        /// Рост (высота)
        /// </summary>
        protected int _height;
        
        /// <summary>
        /// Вес
        /// </summary>
        protected int _weight;

        protected IList<LivingEntity> _children;

        public LivingEntity(int height, int weight)
        {
            _height = height;
            _weight = weight;
            _children = new List<LivingEntity>();
        }

        public IList<LivingEntity> Children
        {
            get => _children;
        }
    }
}