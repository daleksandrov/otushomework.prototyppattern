﻿using System;

namespace OtusHomeWork.PrototypPattern
{
    /// <summary>
    /// Человек
    /// </summary>
    public class Human : LivingEntity, IMyCloneable<Human>, ICloneable
    {
        /// <summary>
        ///  Имя
        /// </summary>
        protected string _name;
        public string Name
        {
            get => _name;
            set => _name = value;
        }


        /// <summary>
        /// Страна
        /// </summary>
        protected string _country;
        public string Country
        {
            get => _country;
            set => _country = value;
        }
        
        public Human(string name, int height, int weight, string country) 
            : base( height, weight)
        {
            _name = name;
            _country = country;
        }
        
        public Human Copy()
        {
            return new Human(_name,_height,_weight, _country);
        }

        public override string ToString()
        {
            return $"Human: {_name}, {_country}, {_height}, {_weight}";
        }

        public object Clone()
        {
            return new Human(_name,_height,_weight, _country);
        }
    }
}