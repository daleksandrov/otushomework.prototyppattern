﻿using System;

namespace OtusHomeWork.PrototypPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Демонстрация работы с пользовательским интерфейсом IMyCloneable ");
            TestMyCloneble();
            
            Console.WriteLine("Демонстарция работы со стандартным интерфейсом IClonable");
            TestStandartCloneble();
        }

        /// <summary>
        /// Демонстрация работы с пользовательским интерфейсом IMyCloneable 
        /// </summary>
        private static void TestMyCloneble()
        {
            Human human1 = new Human("Лена", 172, 48, "Russia");
            Console.WriteLine(human1);

            Human human2 = human1.Copy();
            human2.Name = "Вася";
            Console.WriteLine(human2);


            HerbivoreAnimal herbivoreAnimal1 = new HerbivoreAnimal(150, 45, "Антилопа", false);
            Console.WriteLine(herbivoreAnimal1);

            HerbivoreAnimal herbivoreAnimal2 = herbivoreAnimal1.Copy();
            herbivoreAnimal2.Family = "Корова";
            herbivoreAnimal2.IsPet = true;
            Console.WriteLine(herbivoreAnimal2);


            PredatorAnimal predatorAnimal1 = new PredatorAnimal(150, 45, "Лев", 56);
            Console.WriteLine(predatorAnimal1);

            PredatorAnimal predatorAnimal2 = predatorAnimal1.Copy();
            predatorAnimal2.NumberOfTeeth = 60;
            predatorAnimal2.Family = "Тигр";
            Console.WriteLine(predatorAnimal2);
        }
        
        /// <summary>
        /// Демонстарция работы со стандартным интерфейсом IClonable
        /// </summary>
        private static void TestStandartCloneble()
        {
            Human human1 = new Human("Лена", 172, 48, "Russia");
            Console.WriteLine(human1);

            Human human2 = (Human)human1.Clone();
            human2.Name = "Вася";
            Console.WriteLine(human2);


            HerbivoreAnimal herbivoreAnimal1 = new HerbivoreAnimal(150, 45, "Антилопа", false);
            Console.WriteLine(herbivoreAnimal1);

            HerbivoreAnimal herbivoreAnimal2 = (HerbivoreAnimal)herbivoreAnimal1.Clone();
            herbivoreAnimal2.Family = "Корова";
            herbivoreAnimal2.IsPet = true;
            Console.WriteLine(herbivoreAnimal2);


            PredatorAnimal predatorAnimal1 = new PredatorAnimal(150, 45, "Лев", 56);
            Console.WriteLine(predatorAnimal1);

            PredatorAnimal predatorAnimal2 = (PredatorAnimal)predatorAnimal1.Clone();
            predatorAnimal2.NumberOfTeeth = 60;
            predatorAnimal2.Family = "Тигр";
            Console.WriteLine(predatorAnimal2);
        }
    }
}