﻿namespace OtusHomeWork.PrototypPattern
{
    /// <summary>
    /// Травоядное животное
    /// </summary>
    public class HerbivoreAnimal : Animal, IMyCloneable<HerbivoreAnimal>
    {
        /// <summary>
        /// Домашнее или нет
        /// </summary>
        private bool _isPet;
        public bool IsPet
        {
            get => _isPet;
            set => _isPet = value;
        }

        public HerbivoreAnimal(int height, int weight, string family, bool isPet) 
            : base( height, weight, family)
        {
            _isPet = isPet;
        }
        
        public HerbivoreAnimal Copy()
        {
            return new HerbivoreAnimal( _height, _weight, _family, _isPet);
        }
        
        public override string ToString()
        {
            return $"HerbivoreAnimal: {_family}, {_isPet}, {_height}, {_weight}";
        }

        public override object Clone()
        {
            return new HerbivoreAnimal( _height, _weight, _family, _isPet);
        }
    }
}