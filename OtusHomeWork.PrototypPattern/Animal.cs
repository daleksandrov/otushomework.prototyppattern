﻿using System;

namespace OtusHomeWork.PrototypPattern
{
    /// <summary>
    /// Животное
    /// </summary>
    public class Animal : LivingEntity, IMyCloneable<Animal>, ICloneable
    {
        /// <summary>
        /// Семейство
        /// </summary>
        protected string _family;
        public string Family
        {
            get => _family;
            set => _family = value;
        }

        public Animal( int height, int weight, string family) 
            : base( height, weight)
        {
            _family = family;
        }

        public Animal Copy()
        {
            return new Animal( _height, _weight, _family);
        }
        
        public override string ToString()
        {
            return $"Animal: {_family}, {_height}, {_weight}";
        }

        public virtual object Clone()
        {
            return new Animal( _height, _weight, _family);
        }
    }
}